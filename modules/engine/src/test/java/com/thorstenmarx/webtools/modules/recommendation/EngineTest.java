/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.assertj.core.api.Assertions;
import org.easymock.EasyMock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



/**
 *
 * @author marx
 */
public class EngineTest {
	
	DescriptionService service;
	MockAnalyticsDB analyticsDb = new MockAnalyticsDB();
	
	private Engine engine;
	
	@BeforeClass
	private void setup () {
		service = EasyMock.createNiceMock(DescriptionService.class);
		
		Multimap<String, String> data = ArrayListMultimap.create();
		user(data, "u1", "p1", "p2", "p3", "p4");
		user(data, "u2", "p1", "p4");
		user(data, "u3", "p1", "p2", "p4");
		user(data, "u4", "p2", "p3");
		user(data, "u5", "p1", "p3", "p4");
	
		analyticsDb.documents(createDocuments(data));
		
		RecommendationDescription userDescription = new RecommendationDescription()
				.setId("user")
				.setType(RecommendationDescription.Type.User)
				.setName("user also bought")
				.setItemIdField("c_order_items")
				.setUserIdField("user");
		TimeRange timeRange = new TimeRange().setCount(2).setInterval(Interval.DAY);
		userDescription.setTimeRange(timeRange);
		RecommendationDescription itemDescription = new RecommendationDescription()
				.setId("item")
				.setType(RecommendationDescription.Type.Item)
				.setName("user also bought")
				.setItemIdField("c_order_items")
				.setUserIdField("user");
		itemDescription.setTimeRange(timeRange);
	
		List<RecommendationDescription> recommendations = new ArrayList<>();
		recommendations.add(itemDescription);
		recommendations.add(userDescription);
		EasyMock.expect(service.all()).andReturn(recommendations);
		
		EasyMock.replay(service);
	}
	

	@Test
	public void testInit() {
		
		engine = new Engine(service, analyticsDb);
		engine.init();
	}

	@Test(dependsOnMethods = "testInit")
	public void testUserRecommendation() {
		List<String> recommendations = engine.recommendation("user").recommend("u4", 3);
		Assertions.assertThat(recommendations).contains("p1", "p4").hasSize(2);
	}
	@Test(dependsOnMethods = "testInit")
	public void testItemRecommendation() {
		List<String> recommendations = engine.recommendation("item").recommend("p1", 3);
		Assertions.assertThat(recommendations).contains("p2", "p3", "p4").hasSize(3);
	}

	
	@Test(dependsOnMethods = {"testItemRecommendation", "testUserRecommendation"})
	public void testClose() {
		engine.close();
	}
	
	
	private void user (Multimap<String, String> map, final String user, final String... items) {
		map.putAll(user, Arrays.asList(items));
	}

	public static List<ShardDocument> createDocuments(final Multimap<String, String> data) {
		List<ShardDocument> documents = new ArrayList<>();

		for (final String userid : data.keySet()) {
			Document doc = new Document();
			doc.add(new StringField("user", userid, Field.Store.YES));

			JSONObject source = new JSONObject();
			source.put("user", userid);
			
			JSONArray array = new JSONArray();
			for (final String itemid : data.get(userid)) {
				doc.add(new StringField("c_order_items", itemid, Field.Store.YES));
				array.add(itemid);
			}
			source.put("c_order_items", array);
			
			doc.add(new StringField("_source", source.toJSONString(), Field.Store.YES));

			documents.add(new ShardDocument("shard1", doc));
		}

		return documents;
	}
}
