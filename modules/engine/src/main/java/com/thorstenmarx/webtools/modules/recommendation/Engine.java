/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation;

import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author marx
 */
public class Engine implements DescriptionService.ChangedEventListener {

	final DescriptionService service;
	final AnalyticsDB analyticsDb;

	private final Map<String, Recommendation> recommendations = new ConcurrentHashMap<>();

	public Engine(final DescriptionService service, final AnalyticsDB analyticsDb) {
		this.service = service;
		this.analyticsDb = analyticsDb;
		this.service.addEventListener(this);
	}

	public void init() {
		service.all().stream().forEach((desc) -> {
			recommendations.put(desc.getId(), new Recommendation(desc, analyticsDb));
		});
	}

	public void close() {
		recommendations.values().forEach((r) -> {
			r.close();
		});
		recommendations.clear();
	}
	public void refresh() {
		recommendations.values().forEach((r) -> {
			r.refresh();
		});
	}

	public Recommendation recommendation(final String id) {
		return recommendations.get(id);
	}

	@Override
	public void changed(DescriptionService.ChangedEvent event) {
		if (event.type().equals(DescriptionService.ChangedEvent.Type.Delete)) {
			Recommendation rec = this.recommendations.remove(event.description().getId());
			rec.close();
		} else if (event.type().equals(DescriptionService.ChangedEvent.Type.Update)) {
			// new or update
			if (this.recommendations.containsKey(event.description.getId())) {
				// remove old recommendation
				Recommendation rec = this.recommendations.remove(event.description().getId());
				rec.close();
			}
			Recommendation recommendation = new Recommendation(event.description, analyticsDb);
			this.recommendations.put(recommendation.description.getId(), recommendation);
		}
	}

}
