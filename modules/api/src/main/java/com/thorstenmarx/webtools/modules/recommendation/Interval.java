/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation;

import com.thorstenmarx.webtools.api.TimeUnits;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author marx
 */
@XmlType
@XmlEnum(String.class)
public enum Interval implements Serializable {
	@XmlEnumValue("day") DAY, 
	@XmlEnumValue("week") WEEK, 
	@XmlEnumValue("month") MONTH, 
	@XmlEnumValue("year") YEAR;
	
	
	public TimeUnits.UNIT getTimeUnit () {
		switch (this) {
			case DAY:
				return TimeUnits.UNIT.DAY;
			case WEEK:
				return TimeUnits.UNIT.WEEK;
			case MONTH:
				return TimeUnits.UNIT.MONTH;
			case YEAR:
				return TimeUnits.UNIT.YEAR;
		}
		throw new IllegalArgumentException("no valid interval");
	}
}
