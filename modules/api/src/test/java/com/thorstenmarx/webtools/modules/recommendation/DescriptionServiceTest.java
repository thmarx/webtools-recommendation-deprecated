/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation;

import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;


/**
 *
 * @author marx
 */
public class DescriptionServiceTest {
	
	DescriptionService service;

	@Test
	public void openNoneExistingService() {
		service = new DescriptionService("build/");
	}

	@Test(dependsOnMethods = "openNoneExistingService")
	public void addASimpleDescription() {
		RecommendationDescription description = new RecommendationDescription();
		description.setId("1").setName("recommendation eins").setItemIdField("itemID").setUserIdField("user");
		TimeRange timeRange = new TimeRange();
		timeRange.setInterval(Interval.WEEK).setCount(2);
		description.setTimeRange(timeRange);
		
		service.add(description);
	}
	@Test(dependsOnMethods = "addASimpleDescription")
	public void addAnotherSimpleDescription() {
		RecommendationDescription description = new RecommendationDescription();
		description.setId("2").setName("24h topsellers").setItemIdField("itemID").setUserIdField("user");
		TimeRange timeRange = new TimeRange();
		timeRange.setInterval(Interval.DAY).setCount(1);
		description.setTimeRange(timeRange);
		
		description.getFilter().put("type", "test");
		
		service.add(description);
	}
	@Test(dependsOnMethods = "addAnotherSimpleDescription")
	public void reopenExistingSerivce() {
		service = null;
		service = new DescriptionService("build/");
		
		Assertions.assertThat(service.all().size()).isEqualTo(2);
	}
	
	
	
}
