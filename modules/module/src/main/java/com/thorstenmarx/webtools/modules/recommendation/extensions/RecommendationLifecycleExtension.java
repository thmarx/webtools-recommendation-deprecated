/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.extensions;

import com.thorstenmarx.modules.api.ModuleLifeCycleExtension;
import com.thorstenmarx.modules.api.Services;
import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.api.Lookup;
import com.thorstenmarx.webtools.api.WebToolsContext;
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.DescriptionService;
import com.thorstenmarx.webtools.modules.recommendation.Engine;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
@Extension(ModuleLifeCycleExtension.class)
public class RecommendationLifecycleExtension extends ModuleLifeCycleExtension {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecommendationLifecycleExtension.class);

	ScheduledFuture<?> refreshTask;

	@Override
	public void activate() {
		DescriptionService service = new DescriptionService(configuration.getDataDir().getAbsolutePath());
		Constants.setDescriptionService(service);

		Engine engine = new Engine(service, Services.getDefault().single(AnalyticsDB.class).get());
		engine.init();
		Constants.setEngine(engine);

		refreshTask = WebToolsContext.get().scheduler().scheduleAtFixedRate(() -> {
			LOGGER.debug("refreshing recommendations");
			Constants.getEngine().refresh();
		}, 0, 1, TimeUnit.MINUTES);
		LOGGER.debug("recommendation module activated");
	}

	@Override
	public void deactivate() {
		refreshTask.cancel(false);
		Constants.setDescriptionService(null);
		Constants.getEngine().close();
		LOGGER.debug("recommendation module deactivated");
	}

	@Override
	public void init() {

	}

}
