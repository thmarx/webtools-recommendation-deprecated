/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.pages.config.recommendation;

import com.thorstenmarx.webtools.api.ui.GenericPageBuilder;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.DescriptionService;
import com.thorstenmarx.webtools.modules.recommendation.RecommendationDescription;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

public class RecommendationsPage extends Panel {

	private static final long serialVersionUID = -2053979078406890879L;

	private final GenericPageBuilder pageBuilder;
	
	public RecommendationsPage(String panelId, GenericPageBuilder pageBuilder) {
		super(panelId);
		this.pageBuilder = pageBuilder;
		
		initGui();
	}

	private void initGui() {
		addRecommendationsmodule();
		addCreateNewRecommendationLink();
	}

	private void addRecommendationsmodule() {
		ListView<RecommendationDescription> recommendationView = new ListView<RecommendationDescription>("recommendations", createModelForRecommendations()) {
			private static final long serialVersionUID = 9101744072914090143L;
			@Override
			protected void populateItem(final ListItem<RecommendationDescription> item) {
				item.add(new Label("id", new PropertyModel<>(item.getModel(), "id")));
				item.add(new Label("name", new PropertyModel<>(item.getModel(), "name")));

				Link<WebPage> editSiteLink = new Link<WebPage>("editRecommendationLink") {
					private static final long serialVersionUID = -4331619903296515985L;
					@Override
					public void onClick() {
						setResponsePage(pageBuilder.getPage(new AddEditRecommendationPage(Constants.CONTENT_ID, item.getModelObject().getId(), pageBuilder)));
					}
				};

				item.add(editSiteLink);
				item.add(new RemoveRecommendationLink("removeRecommendationLink", item.getModelObject(), pageBuilder));
			}
		};

		recommendationView.setVisible(!recommendationView.getList().isEmpty());
		add(recommendationView);

		Label noRecommendationsLabel = new Label("noRecommendationsLabel", "There are no recommendations in the database. Maybe you can add one?");
		noRecommendationsLabel.setVisible(!recommendationView.isVisible());
		add(noRecommendationsLabel);

	}

	private LoadableDetachableModel<List<RecommendationDescription>> createModelForRecommendations() {

		return new LoadableDetachableModel<List<RecommendationDescription>>() {
			private static final long serialVersionUID = 5275935387613157437L;
			@Override
			protected List<RecommendationDescription> load() {
				return new ArrayList<>(Constants.getDescriptionService().all());
			}

		};
	}

	private void addCreateNewRecommendationLink() {
		add(new Link<WebPage>("addRecommendationPageLink") {

			@Override
			public void onClick() {
				setResponsePage(pageBuilder.getPage(new AddEditRecommendationPage(Constants.CONTENT_ID, pageBuilder)));
			}
		});
	}
	
}
