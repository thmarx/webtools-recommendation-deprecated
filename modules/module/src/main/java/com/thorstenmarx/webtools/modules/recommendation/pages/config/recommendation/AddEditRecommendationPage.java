/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.pages.config.recommendation;

import com.thorstenmarx.webtools.api.ui.GenericPageBuilder;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.Interval;
import com.thorstenmarx.webtools.modules.recommendation.RecommendationDescription;
import java.util.Arrays;
import org.apache.wicket.feedback.ExactLevelFeedbackMessageFilter;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;

public class AddEditRecommendationPage extends Panel {

	private static final long serialVersionUID = 4997468161164832069L;

	private final GenericPageBuilder pageBuilder;

	private boolean edit = false;
	
	public AddEditRecommendationPage(String panelId, GenericPageBuilder pageBuilder) {
		super(panelId);
		this.edit = false;
		this.pageBuilder = pageBuilder;
		
		setDefaultModel(new Model<>(new RecommendationDescription()));
		initGui();
	}
	public AddEditRecommendationPage(String panelId, final String recommendationId, GenericPageBuilder pageBuilder) {
		super(panelId);
		this.edit = false;
		this.pageBuilder = pageBuilder;
		
		RecommendationDescription description = Constants.getDescriptionService().get(recommendationId);
		setDefaultModel(new Model<>(description));
		initGui();
	}
	
	private void initGui() {

		Form<RecommendationDescription> addLocationForm = new Form<>("addRecommendationForm",
				new CompoundPropertyModel<RecommendationDescription>((IModel<RecommendationDescription>) getDefaultModel()));
		add(addLocationForm);

		addLocationForm.add(createLabelFieldWithValidation("name", "recommendationName"));
		RequiredTextField<String> idField = createLabelFieldWithValidation("id", "recommendationId");
		if (edit) {
			idField.setEnabled(false);
		}
		addLocationForm.add(idField);
		addLocationForm.add(createLabelFieldWithValidation("event", "recommendationEvent"));
		addLocationForm.add(createLabelFieldWithValidation("userIdField", "recommendationUID"));
		addLocationForm.add(createLabelFieldWithValidation("itemIdField", "recommendationIID"));
		
		
		//Initialize the list of persons here…
		ChoiceRenderer<RecommendationDescription.Type> typeRenderer = new ChoiceRenderer<>("name");
		addLocationForm.add(new DropDownChoice<>("type", new PropertyModel<>(getDefaultModelObject(), "type"), Arrays.asList(RecommendationDescription.Type.values()), typeRenderer));
		
		ChoiceRenderer<Interval> intervalRenderer = new ChoiceRenderer<>("name");
		addLocationForm.add(new DropDownChoice<>("timerange-interval", new PropertyModel<>(getDefaultModelObject(), "timeRange.interval"), Arrays.asList(Interval.values()), intervalRenderer));
		addLocationForm.add(new TextField("timerange-count", new PropertyModel<>(getDefaultModelObject(), "timeRange.count")));

		Button submitButton = new Button("submitButton") {
			@Override
			public void onSubmit() {
				RecommendationDescription recommendation = getRecommendationFromPageModel();

				Constants.getDescriptionService().add(recommendation);
				getSession().info(new StringResourceModel("recommendationUpdated", this).getString());

				setResponsePage(pageBuilder.getPage(new RecommendationsPage(Constants.CONTENT_ID, pageBuilder)));
			}
		};
		addLocationForm.add(submitButton);
		
		add(new FeedbackPanel("feedbackErrors", new ExactLevelFeedbackMessageFilter(FeedbackMessage.ERROR)));
	}

	private RequiredTextField<String> createLabelFieldWithValidation(String id, String property) {
		RequiredTextField<String> nameField = new RequiredTextField<>(id);
		nameField.setLabel(new StringResourceModel(property, this));

		return nameField;
	}

	@SuppressWarnings("unchecked")
	private RecommendationDescription getRecommendationFromPageModel() {
		return (RecommendationDescription) getDefaultModel().getObject();
	}
}
