/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.extensions;

import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.api.extensions.ManagerConfigExtension;
import com.thorstenmarx.webtools.api.ui.GenericPageBuilder;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.pages.config.recommendation.RecommendationsPage;
import org.apache.wicket.markup.html.WebPage;

/**
 *
 * @author marx
 */
@Extension(ManagerConfigExtension.class)
public class RecommendationConfigExtension extends ManagerConfigExtension {

	@Override
	public String getTitle() {
		return "Recommendations";
	}

	@Override
	public void init() {
	}

	@Override
	public WebPage getPage(GenericPageBuilder pageBuilder) {
		return pageBuilder.getPage(new RecommendationsPage(Constants.CONTENT_ID, pageBuilder));
	}
	
}
