/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.pages.config.recommendation;



import com.thorstenmarx.webtools.api.ui.GenericPageBuilder;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.DescriptionService;
import com.thorstenmarx.webtools.modules.recommendation.RecommendationDescription;
import com.thorstenmarx.webtools.modules.recommendation.pages.utils.ConfirmationLink;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

/**
 *
 * @author marx
 */
public class RemoveRecommendationLink extends ConfirmationLink<Void> {
	
	private final RecommendationDescription recommendation;
	private GenericPageBuilder pageBuilder;

	public RemoveRecommendationLink(String componentId, RecommendationDescription recommendation, GenericPageBuilder pageBuilder) {
		super(componentId, "Delete recommendation?");
		this.recommendation = recommendation;
		this.pageBuilder = pageBuilder;
	}

	

	@Override
	public void onClick(AjaxRequestTarget art) {
		
		Constants.getDescriptionService().remove(this.recommendation.getId());
		getSession().info(new StringResourceModel("recommendatinoRemoved", Model.of(recommendation.getName())));
        setResponsePage(pageBuilder.getPage(new RecommendationsPage(Constants.CONTENT_ID, pageBuilder)));
	}

}
