/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation.extensions;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.api.extensions.RestCommandProcessorExtension;
import com.thorstenmarx.webtools.modules.recommendation.Constants;
import com.thorstenmarx.webtools.modules.recommendation.Recommendation;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author marx
 */
@Extension(RestCommandProcessorExtension.class)
public class RecommendationRestExtension extends RestCommandProcessorExtension {

	@Override
	public void init() {
	}

	@Override
	public JSONObject get(String string, HttpServletRequest request) {
		JSONObject result = new JSONObject();

		final String rec = request.getParameter("recommendation");
		final String [] ids = request.getParameterValues("id");
		
		if (Strings.isNullOrEmpty(rec) || (ids == null || ids.length == 0)) {
			return result;
		}
		
		final String countParameter = request.getParameter("count");
		int count = 5;
		if (!Strings.isNullOrEmpty(countParameter) && isInteger(countParameter)) {
			count = Integer.parseInt(countParameter);
		}
		final Recommendation recommendation = Constants.getEngine().recommendation(rec);

		if (recommendation != null) {
			JSONArray recommendationsArray = new JSONArray();
			for (final String id : ids) {
				JSONObject recommendationsObject = new JSONObject();
				List<String> recommendations = recommendation.recommend(id, count);
				recommendationsObject.put("recommendations", recommendations);
				recommendationsObject.put("id", id);
				recommendationsArray.add(recommendationsObject);
			}
			result.put("recommendations", recommendationsArray);
		} else {
			result.put("error", true);
			result.put("message", "recommendation + '" + rec + "' not found");
		}
		
		
		return result;
	}

	@Override
	public JSONObject post(String string, HttpServletRequest hsr) {
		JSONObject result = new JSONObject();

		return result;
	}

	private boolean isInteger(final String str) {
		try {
			int num = Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			// not an integer!
		}
		return false;
	}
}
