/**
 * WebTools-Recommendation
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.recommendation;

/**
 *
 * @author marx
 */
public abstract class Constants {
	public static final String CONTENT_ID = "contentComponent";
	
	private static DescriptionService descriptionService;
	
	private static Engine engine;

	public static Engine getEngine() {
		return engine;
	}

	public static void setEngine(Engine engine) {
		Constants.engine = engine;
	}
	
	

	public static DescriptionService getDescriptionService() {
		return descriptionService;
	}

	public static void setDescriptionService(DescriptionService descriptionService) {
		Constants.descriptionService = descriptionService;
	}
	
	
}
